- Récupérer des vidéos de bonnes qualités: https://www.pexels.com/search/videos/open%20source/ ou sur YouTube avec mon script
- Choix d'un algorithme de tatouage des frames: 
	- lsb(ne résiste pas à la compression), quantification, étalement de spectre
	- Pouvoir lire le tatouage
- FFmpeg:
	- permet de faire de la compression d'image pour réaliser l'attaque

- Utilisation de  Python+Jupyter notebook

- Procédure de tatouage
    - tatouage de toutes les frames par un symbole a => video a, calcul de la PSNR
    - tatouage de toutes les frames par un autre symbole b => video b, calcul de la PSNR
    - puis mélange des frames => vidéo vendue, calcul de la PSNR
    - lecture du tatouage de la vidéo vendue
    - compression par les pirates ~ attaque
    - lecture du tatouage de la vidéo compressée disponible sur une plateforme pirate
