import sys
import math
import random
from PIL import Image as PILImage
import numpy as np
from scipy.fftpack import dct, idct


N = 32
NDCT = 12
ALPHA = 1.04

# Les coefficients DCT qui seront marqués
C = [1, 2, 3, 4, 8, 9, 10, 11, 16, 17, 18, 24]

def apply_dct(image):
    return dct(dct(image.T, norm='ortho').T, norm='ortho')

def apply_idct(dct_coeffs):
    return idct(idct(dct_coeffs.T, norm='ortho').T, norm='ortho')

def embed_message(src_data, message):
    message = int(message)

    # Génération du message
    M = [(message >> i) % 2 for i in range(N)]

    random.seed(78425)

    # Transformée DCT de l'image hôte
    src_dct_data = apply_dct(src_data)

    # Génération du vecteur hôte
    X = [None] * (NDCT * src_data.size // 64)
    cur = 0

    for by in range(src_data.shape[1] // 8):
        for bx in range(src_data.shape[0] // 8):
            for i in range(NDCT):
                x = 8 * bx + C[i] % 8
                y = 8 * by + C[i] // 8
                X[cur] = src_dct_data[x, y]
                cur += 1

    # Tatouage
    for i in range(len(X)):
        wi = 0
        for j in range(N):
            if random.choice([True, False]):
                Gij = -1
            else:
                Gij = 1

            if M[j] == 0:
                M[j] = -1
            wi += Gij * M[j]

        wi *= ALPHA
        X[i] += wi

    # Transformée inverse
    modified_dct = src_dct_data.copy()
    cur = 0
    for by in range(src_data.shape[1] // 8):
        for bx in range(src_data.shape[0] // 8):
            for i in range(NDCT):
                x = 8 * bx + C[i] % 8
                y = 8 * by + C[i] // 8
                modified_dct[x, y] = X[cur]
                cur += 1

    dst_data = apply_idct(modified_dct)
    dst_image = PILImage.fromarray(dst_data.astype('uint8'))

    # Calcul du PSNR
    eqm = ((dst_data - src_data) ** 2).mean()
    psnr = 10 * math.log10(255 ** 2 / eqm)
    print(f"EQM = {eqm} | PSNR = {psnr} dB")

    # Sauvegarde de l'image marquée
    return dst_image


if __name__ == "__main__":
    input_file = r"data/test/camion.pgm"
    src_image = PILImage.open(input_file)
    src_data = np.array(src_image)

    output_file = r"data/test/new_camion.pgm"
    message = 11
    embed_message(src_data, message).save(output_file)