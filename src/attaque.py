import subprocess

def compress_video(input_file, output_file, crf=90):
    """
    Compress a video using ffmpeg.

    Parameters:
    - input_file: The path to the input video file.
    - output_file: The path to the output compressed video file.
    - crf: Constant Rate Factor (default is 23, lower values mean better quality but larger file size).

    Note: You can customize the ffmpeg command based on your specific requirements.
    """
    ffmpeg_cmd = [
        'ffmpeg',
        '-i', input_file,
        '-c:v', 'libx264',  # Video codec
        '-crf', str(crf),
        '-preset', 'medium',  # Encoding speed vs compression efficiency
        '-c:a', 'aac',  # Audio codec
        '-strict', 'experimental',  # Required for some AAC encoding
        '-b:a', '128k',  # Audio bitrate
        output_file
    ]

    try:
        subprocess.run(ffmpeg_cmd, check=True)
        print(f'Compression successful. Output saved to: {output_file}')
    except subprocess.CalledProcessError as e:
        print(f'Error during compression: {e}')

# Exemple d'utilisation
input_video = 'data/videos/in.mp4'
output_video = 'data/videos/in_compressed.mp4'
compress_video(input_video, output_video)
