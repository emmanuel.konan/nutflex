from PIL import Image

def convert_to_pgm(input_path, output_path):
    # Open the image
    img = Image.open(input_path)

    # Convert the image to grayscale
    img_gray = img.convert('L')

    # Save the grayscale image in PGM format
    img_gray.save(output_path)

# Example usage
input_image_path = r"data/images/flowers.jpg"
output_pgm_path = r'data/test/new2.pgm'

convert_to_pgm(input_image_path, output_pgm_path)
