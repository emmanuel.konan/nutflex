from pytube import YouTube

def download(link):
    yt = YouTube(link)
    yd = yt.streams.get_highest_resolution()
    yd.download(
        "."
    )

    print("Title: ", yt.title)
    print("View: ", yt.views)

    # ADD FOLDER HERE


if __name__ == "__main__":
    link = str(input("Saisir le lien de la vidéo YouTube s'il vous plaît : "))
    download(link)


