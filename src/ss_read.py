import math
import random
from PIL import Image as PILImage
import numpy as np
from scipy.fftpack import dct, idct

N = 32
NDCT = 12
ALPHA = 1.04

# Les coefficients DCT qui seront marqués
C = [1, 2, 3, 4, 8, 9, 10, 11, 16, 17, 18, 24]

def apply_dct(image):
    return dct(dct(image.T, norm='ortho').T, norm='ortho')

def apply_idct(dct_coeffs):
    return idct(idct(dct_coeffs.T, norm='ortho').T, norm='ortho')



def read_message(src_data):
    
    random.seed(78425)

    # Transformée DCT de l'image hôte
    src_dct_data = apply_dct(src_data)

    # Génération du vecteur hôte
    X = [None] * (NDCT * src_data.size // 64)
    cur = 0

    for by in range(src_data.shape[1] // 8):
        for bx in range(src_data.shape[0] // 8):
            for i in range(NDCT):
                x = 8 * bx + C[i] % 8
                y = 8 * by + C[i] // 8
                X[cur] = src_dct_data[x, y]
                cur += 1

   # Lecture du tatouage
    M = [0 for i in range(N)]
    Gij_values = []
    for i in range(len(X)):
        for j in range(N):
            if random.randint(0, 1) == 0:
                Gij = -1
            else:
                Gij = 1
            M[j] += Gij * X[i]
            Gij_values.append(Gij)

    # Affichage du message
    message = sum(1 << i for i in range(N) if M[i] > 0)
    print(f"Message : {message}")


    # Quelques statistiques sur le canal
    var = 0
    moy = 0

    for i in range(N):
        if M[i] > 0:
            moy += M[i]
        else:
            moy -= M[i]

    for i in range(N):
        if M[i] > 0:
            var += (M[i] - moy / N)**2
        else:
            var += (M[i] + moy / N)**2

    snr = (moy / N)**2 / (var / N)
    print(f"SNR = {snr}")

    return


if __name__ == "__main__":
    input_file = r"data/test/new_camion.pgm"
    src_image = PILImage.open(input_file)
    src_data = np.array(src_image)

    read_message(src_data)