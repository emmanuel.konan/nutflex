import numpy as np

def lsb_write(image_array, zeroORone, watermark_size=20):
    # Define the region for watermarking (top-left corner)
    watermark_region = (slice(0, watermark_size), slice(0, watermark_size), slice(None))

    # Clear and set the two LSBs only in the specified region
    image_array[watermark_region] &= 0b1111111111111100
    if zeroORone == 1:
        image_array[watermark_region] |= 0b11
    else:
        image_array[watermark_region] |= 0b00


def lsb_read(image_array, watermark_size=20):
    # Define the region for watermark reading (top-left corner)
    watermark_region = (slice(0, watermark_size), slice(0, watermark_size), slice(None))

    # Extract the two LSBs from the specified region
    extracted_bits = image_array[watermark_region] & 0b11

    # Convert the extracted bits to a single integer
    watermark_value = extracted_bits.ravel().view(np.uint8)[0]

    return watermark_value
